__author__ = 'GHajba'

from urllib2 import urlopen, Request
import json
import os
from threading import Thread
from Queue import Queue


class Downloader(Thread):
    def __init__(self, queue, folder):
        Thread.__init__(self)
        self.queue = queue
        self.folder = folder

    def run(self):
        while True:
            url = self.queue.get()
            download_images(self.folder, url)
            self.queue.task_done()


def get_image_urls(client_id):
    headers = {'Authorization': 'Client-ID {0}'.format(client_id)}
    result = []
    for i in range(1):
        data = json.loads(
            urlopen(Request('https://api.imgur.com/3/gallery/hot/viral/{0}.json'.format(i), headers=headers)).read())
        result += map(lambda image: image['link'], data['data'])
    return result


def download_images(target_dir, url):
    path = target_dir + '/' + os.path.basename(url)

    with open(path, 'wb') as file:
        file.write(urlopen(url).read())


if __name__ == '__main__':
    from time import time
    import sys

    queue = Queue()
    start = time()
    image_urls = [u for u in get_image_urls(os.getenv('IMGUR_CLIENT_ID')) if u.endswith('.jpg')]
    for i in range(4):
        downloader = Downloader(queue, 'images')
        downloader.daemon = True
        downloader.start()
    for url in image_urls:
        queue.put(url)
    queue.join()
    print 'Downloaded {0} images in {1} seconds with Python {2}'.format(len(image_urls), time() - start,
                                                                        sys.version_info[0])
