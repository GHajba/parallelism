__author__ = 'GHajba'

from urllib2 import urlopen, Request
import json
import os


def get_image_urls(client_id):
    headers = {'Authorization': 'Client-ID {0}'.format(client_id)}
    data = json.loads(urlopen(Request('https://api.imgur.com/3/gallery/hot/viral/', headers=headers)).read())
    return map(lambda image: image['link'], data['data'])


def download_images(target_dir, url):
    path = target_dir + '/' + os.path.basename(url)

    with open(path, 'wb') as file:
        file.write(urlopen(url).read())


if __name__ == '__main__':
    from time import time
    import sys
    from multiprocessing.pool import Pool
    from functools import partial

    start = time()
    image_urls = [u for u in get_image_urls(os.getenv('IMGUR_CLIENT_ID')) if u.endswith('.jpg')]
    download = partial(download_images, 'images')
    p = Pool(4)
    p.map(download, image_urls)
    print 'Downloaded {0} images in {1} seconds with Python {2}'.format(len(image_urls), time() - start,
                                                                        sys.version_info[0])
