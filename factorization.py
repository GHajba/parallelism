__author__ = 'GHajba'

import math
from threading import Thread
from Queue import Queue
from multiprocessing.pool import Pool
from functools import partial


def factors(result, n):
    if n <= 1:
        return result
    for i in range(2, n + 1):
        if n % i == 0:
            result.append(i)
            return factors(result, n / i)


class Factorizer(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            n = self.queue.get()
            result = factors([], n)
            self.queue.task_done()


def threaded(numbers, thread_count):
    queue = Queue()
    for i in range(thread_count):
        factorizer = Factorizer(queue)
        factorizer.daemon = True
        factorizer.start()
    for n in numbers:
        queue.put(n)
    queue.join()


def process_factorizer(number, processes):
    factor = partial(factors, [])
    pool = Pool(processes)
    pool.map(factor, number)
    pool.close()


if __name__ == '__main__':
    from time import time
    import sys

    version = '.'.join(map(str, sys.version_info[:3]))
    numbers = range(1, 50001)
    len_numbers = len(numbers)

    start = time()
    for i in numbers:
       factors(i, [])
    print(
       'Factorizing {0} numbers took {1} seconds with serial recursive approach and Python {2}'.format(len_numbers, (time() - start),
                                                                                         version))

    for i in [1, 2, 4, 8, 16, 32, 64, 128]:
        start = time()
        threaded(numbers, i)
        print(
        'Factorizing {0} numbers took {1} seconds with {2} threads and Python {3}'.format(len_numbers, (time() - start),
                                                                                          i, version))
        start = time()
        process_factorizer(numbers, i)
        print(
            'Factorizing {0} numbers took {1} seconds with {2} processes and Python {3}'.format(len_numbers,
                                                                                                (time() - start),
                                                                                                i, version))
