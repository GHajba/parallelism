# Parallelism examples

This repository contains the sources for the "Parallel woring" chapter of my book [Website Scraping with Python](http://leanpub.com/websitescrapingwithpython).

Each example is in its own Python file. The number *3* at the end of the file's name indicates that this is the Python 3 version of the same example.

* *imgur.py* and *imgur_3.py* contain the basic example with a single thread / process
* *threads.py* and *thready_3.py* contain the threaded example
* *processes.py* and *processes_3.py* contain the multi-processed example
* *factorization.py* and *factorization_3.py* contain the CPU-expensive factorization example.

If you have any questions feel free to ask. The description of the code you will find in the book, which is for sale at http://leanpub.com/websitescrapingwithpython

The examples are licensed under the MIT license as stated in the LICENSE.txt file too.