__author__ = 'GHajba'

from urllib.request import urlopen, Request
import json
import os


def get_image_urls(client_id):
    headers = {'Authorization': 'Client-ID {0}'.format(client_id)}
    with urlopen(Request('https://api.imgur.com/3/g/memes/', headers=headers)) as response:
        data = json.loads(response.read().decode('utf-8'))
    print(list(map(lambda image: image['link'], data['data'])))
    return map(lambda image: image['link'], data['data'])


def download_images(target_dir, url):
    path = target_dir + '/' + os.path.basename(url)

    with open(path, 'wb') as file:
        file.write(urlopen(url).read())


if __name__ == '__main__':
    from time import time
    import sys

    start = time()
    image_urls = [u for u in get_image_urls(os.getenv('IMGUR_CLIENT_ID')) if u.endswith('.jpg') or u.endswith('.png')]
    for url in image_urls:
        download_images('images', url)
    print(
    'Downloaded {0} images in {1} seconds with Python {2}'.format(len(image_urls), time() - start, sys.version_info[0]))
